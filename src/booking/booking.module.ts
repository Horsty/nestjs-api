import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanetModule } from 'src/planet/planet.module';
import { StarshipModule } from 'src/starship/starship.module';
import { BookingController } from './booking.controller';
import { BookingService } from './booking.service';
import { Booking } from './entities/booking.entity';

@Module({
  controllers: [BookingController],
  imports: [PlanetModule, StarshipModule, TypeOrmModule.forFeature([Booking])],
  providers: [BookingService],
})
export class BookingModule {}
